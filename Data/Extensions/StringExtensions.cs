﻿using Newtonsoft.Json;

namespace Data.Extensions
{
    public static class StringExtensions
    {
        public static T? DeserializeObject<T>(this string str) => JsonConvert.DeserializeObject<T>(str);
    }
}
