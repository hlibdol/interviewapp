﻿using Newtonsoft.Json;

namespace Data.Extensions
{
    public static class ObjectExtensions
    {
        public static string SerializeObject(this object obj) => JsonConvert.SerializeObject(obj);
    }
}
