﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Data.Extensions
{
    public static class IEnumarableExtensions
    {
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> enumerable)
        {
            foreach (T? item in enumerable)
            {
                collection.Add(item);
            }
        }
    }
}
