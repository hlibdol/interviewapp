using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Data.Constants;
using Domain.Entities;
using Domain.Repositories;
using Newtonsoft.Json;

namespace Data.Repositories
{
    public class DemoFriendsRepository : IFriendsRepository
    {
        public async Task<IEnumerable<Friend>> GetFriends(CancellationToken cancellationToken = default)
        {
            try
            {
                using var httpClient = new HttpClient();

                var response = await httpClient.GetAsync(ApiUrls.FriendsRepositoryUrl, cancellationToken);

                var strData = await response.Content.ReadAsStringAsync();

                var friendsData = JsonConvert.DeserializeObject<FriendsWrapper>(strData);

                return friendsData?.Friends!;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}