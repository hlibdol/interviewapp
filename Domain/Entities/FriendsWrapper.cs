﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class FriendsWrapper
    {
        public List<Friend>? Friends { get; set; }
    }
}
