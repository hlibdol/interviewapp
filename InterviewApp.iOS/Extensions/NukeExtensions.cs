using Foundation;
using UIKit;
using Xamarin.Nuke;

namespace InterviewApp.iOS.Extensions
{
    public static class NukeExtensions
    {
        public static void LoadImage(this UIImageView imageView, string imageUrl) =>
            ImagePipeline.Shared?.LoadImageWithUrl(new NSUrl(imageUrl), null, null, imageView);
    }
}