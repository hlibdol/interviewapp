﻿using System;
using Domain.Entities;

namespace Presentation.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        public string ProfilePictureUrl { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }

        public void SetData(Friend friend)
        {
            ProfilePictureUrl = friend.ProfilePictureUrl;
            Name = $"{friend.FirstName} {friend.LastName}";
            NickName = friend.NickName;
            Age = $"{CalculateAge(friend.DateOfBirth)} yrs";
            Description = $"You can play recorded Virtual Golf rounds with {friend.FirstName} in TrackMan Performance Studio (TPS). More Friends functionalities will be available soon.";
        }

        private int CalculateAge(DateTime dateOfbirth)
        {
            var today = DateTime.Today;

            // Calculate the age.
            var age = today.Year - dateOfbirth.Year;

            // Go back to the year in which the person was born in case of a leap year
            if (dateOfbirth.Date > today.AddYears(-age))
                age--;

            return age;
        }
    }
}
