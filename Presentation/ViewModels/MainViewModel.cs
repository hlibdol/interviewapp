using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Repositories;

namespace Presentation.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IFriendsRepository _friendsRepository;

        public ObservableCollection<SectionViewModel> Sections { get; } = new();
        public string Title { get; } = "FRIENDS";

        public MainViewModel(IFriendsRepository friendsRepository)
        {
            _friendsRepository = friendsRepository;
        }

        public async Task LoadFriends()
        {
            IEnumerable<Friend> friends = await _friendsRepository.GetFriends().ConfigureAwait(false);

            if(friends == null)
            {
                // some validation
                return;
            }

            await Xamarin.Essentials.MainThread.InvokeOnMainThreadAsync(() =>
            {
                Sections.Clear();

                var friendGroups = friends.GroupBy(x => x.IsFriend);

                foreach (var item in friendGroups)
                {
                    Sections.Add(new SectionViewModel(item.Key, item.Select(x => x)));
                }
            }).ConfigureAwait(false);
        }
    }
}