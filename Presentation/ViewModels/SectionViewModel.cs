﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Data.Extensions;
using Domain.Entities;

namespace Presentation.ViewModels
{
    public class SectionViewModel : BaseViewModel
    {
        public ObservableCollection<FriendViewModel> Friends { get; } = new();

        public string SectionTitle { get; } = string.Empty;

        public SectionViewModel(bool sectionKey, IEnumerable<Friend> friends)
        {
            Friends.AddRange(friends.Select(x => new FriendViewModel(x)));

            SectionTitle = sectionKey ? "Friends" : "Recently Played";
        }
    }
}
