using System;
using Android.App;
using Android.Runtime;
using Data.Repositories;
using Domain.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Presentation.ViewModels;
using XEPlatform = Xamarin.Essentials.Platform;

namespace InterviewApp.Droid
{
    [Application(Name = "dk.trackman.interviewapp_droid.MainApplication",
#if DEBUG
        Debuggable = true
#else
        Debuggable = false
#endif
    )]
    public class MainApplication : Application
    {
        public static IServiceProvider? ServiceProvider { get; private set; }

        public MainApplication() : base() { }
        public MainApplication(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer) { }

        public override void OnCreate()
        {
            base.OnCreate();
            XEPlatform.Init(this);

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<MainViewModel>();
            serviceCollection.AddTransient<UserViewModel>();
            serviceCollection.AddSingleton<IFriendsRepository, DemoFriendsRepository>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
    }
}