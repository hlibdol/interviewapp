﻿using System.Collections.ObjectModel;
using Android.Content;
using Android.Views;
using AndroidX.RecyclerView.Widget;
using InterviewApp.Droid.Views.ViewHolders;
using Presentation.ViewModels;

namespace InterviewApp.Droid.Adapters
{
    public class SectionAdapter : RecyclerView.Adapter
    {
        private ObservableCollection<SectionViewModel> _sectionViewModels { get; }
        private LayoutInflater _layoutInflater { get; }
        private readonly Context _context;

        public SectionAdapter(Context context, ObservableCollection<SectionViewModel> sectionViewModels)
        {
            _sectionViewModels = sectionViewModels;

            _context = context;
            _layoutInflater = LayoutInflater.FromContext(context);
        }

        public override int ItemCount => _sectionViewModels?.Count ?? 0;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (holder is SectionViewHolder sectionViewHolder)
            {
                sectionViewHolder.BindViewHolder(_context, _sectionViewModels![position]);
            }
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            => new SectionViewHolder(_layoutInflater!.Inflate(Resource.Layout.item_section, parent, false)!);
    }
}