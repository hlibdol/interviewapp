﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using InterviewApp.Droid.Adapters;
using Presentation.ViewModels;

namespace InterviewApp.Droid.Views.ViewHolders
{
    public class SectionViewHolder : RecyclerView.ViewHolder
    {
        private readonly TextView _sectionTitle;
        private readonly RecyclerView _sectionItems;

        public SectionViewHolder(View itemView) : base(itemView)
        {
            _sectionTitle = itemView.FindViewById<TextView>(Resource.Id.sectionTitle);
            _sectionItems = itemView.FindViewById<RecyclerView>(Resource.Id.recycler_friends);
        }

        public SectionViewHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public void BindViewHolder(Context context, SectionViewModel section)
        {
            _sectionTitle.Text = section.SectionTitle;

            if (_sectionItems != null)
            {
                _sectionItems.SetLayoutManager(new LinearLayoutManager(context) { Orientation = RecyclerView.Vertical });
                _sectionItems.SetAdapter(new FriendsAdapter(context, section.Friends));
            }
        }
    }
}