using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using InterviewApp.Droid.Extensions;
using InterviewApp.Droid.Views.Activities;
using Presentation.ViewModels;
using Data.Extensions;

namespace InterviewApp.Droid.Views.ViewHolders
{
    public sealed class FriendViewHolder : RecyclerView.ViewHolder, View.IOnClickListener
    {
        private readonly TextView? _nameTextView;
        private readonly TextView? _nickNameTextView;
        private readonly ImageView? _profilePicture;

        private FriendViewModel _friendViewModel;

        public FriendViewHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public FriendViewHolder(View itemView) : base(itemView)
        {
            _nameTextView = itemView.FindViewById<TextView>(Resource.Id.nameTextView);
            _nickNameTextView = itemView.FindViewById<TextView>(Resource.Id.nickName);
            _profilePicture = itemView.FindViewById<ImageView>(Resource.Id.profileImageView);
            if (_profilePicture != null)
            {
                _profilePicture.OutlineProvider = new CircleOutlineProvider();
                _profilePicture.ClipToOutline = true;
            }

            itemView.SetOnClickListener(this);
        }

        public void BindViewHolder(FriendViewModel viewModel)
        {
            _profilePicture?.LoadImage(viewModel.ProfilePictureUrl);
            _nameTextView?.SetText(viewModel.Name, TextView.BufferType.Normal);
            _nickNameTextView?.SetText(viewModel.NickName, TextView.BufferType.Normal);

            _friendViewModel = viewModel;
        }

        public void OnClick(View v)
        {
            var context = v.Context;
            var intent = new Intent(context, typeof(UserActivity));
            intent.PutExtra("user", _friendViewModel.Model.SerializeObject());

            context.StartActivity(intent);
        }
    }

    public sealed class CircleOutlineProvider : ViewOutlineProvider
    {
        public override void GetOutline(View? view, Outline? outline)
        {
            outline?.SetRoundRect(0, 0, view?.Width ?? 0, view?.Height ?? 0, (view?.Height ?? 1) / 2f);
        }
    }
}