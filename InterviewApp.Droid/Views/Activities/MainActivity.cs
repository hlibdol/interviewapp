﻿using Android.App;
using Android.OS;
using Android.Runtime;
using AndroidX.AppCompat.Widget;
using AndroidX.AppCompat.App;
using AndroidX.RecyclerView.Widget;
using Microsoft.Extensions.DependencyInjection;
using Presentation.ViewModels;
using InterviewApp.Droid.Adapters;
using System.Threading.Tasks;

namespace InterviewApp.Droid.Views.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private MainViewModel? _viewModel;
        private SectionAdapter _sectionAdapter;

        protected override void OnCreate(Bundle? savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            _viewModel = MainApplication.ServiceProvider?.GetRequiredService<MainViewModel>();

            SetContentView(Resource.Layout.activity_main);

            Toolbar? toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            Title = _viewModel?.Title;

            RecyclerView? recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_sections);
            if (recyclerView != null)
            {
                var linearLayout = new LinearLayoutManager(this) { Orientation = RecyclerView.Vertical };
                var dividerItemDecoration = new DividerItemDecoration(recyclerView.Context, linearLayout.Orientation);
                _sectionAdapter = new SectionAdapter(this, _viewModel?.Sections);

                recyclerView.SetLayoutManager(linearLayout);
                recyclerView.AddItemDecoration(dividerItemDecoration);
                recyclerView.SetAdapter(_sectionAdapter);
            }
        }

        protected override void OnResume()
        {
            base.OnResume();

            _ = ReloadListData();
        }

        private async Task ReloadListData()
        {
            await _viewModel?.LoadFriends();

            _sectionAdapter.NotifyDataSetChanged();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

