﻿using Android.App;
using Android.OS;
using Android.Widget;
using InterviewApp.Droid.Extensions;
using InterviewApp.Droid.Views.ViewHolders;
using Microsoft.Extensions.DependencyInjection;
using Presentation.ViewModels;
using Data.Extensions;
using Domain.Entities;
using Android.Views;

namespace InterviewApp.Droid.Views.Activities
{
    [Activity(Label = "UserActivity")]
    public class UserActivity : Activity, View.IOnClickListener
    {
        private UserViewModel _viewModel;

        private ImageView _userAvatarIv;
        private TextView _userNameTv;
        private TextView _userNicknameTv;
        private TextView _userAgeTv;
        private TextView _descriptionTv;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            _viewModel = MainApplication.ServiceProvider?.GetRequiredService<UserViewModel>();
            var userData = Intent.GetStringExtra("user");
            if(userData.DeserializeObject<Friend>() is Friend friend)
            {
                _viewModel.SetData(friend);
            }

            SetContentView(Resource.Layout.activity_user);

            _userAvatarIv = FindViewById<ImageView>(Resource.Id.user_avatar);
            _userNameTv = FindViewById<TextView>(Resource.Id.user_full_name);
            _userNicknameTv = FindViewById<TextView>(Resource.Id.user_nick_name);
            _userAgeTv = FindViewById<TextView>(Resource.Id.user_age);
            _descriptionTv = FindViewById<TextView>(Resource.Id.user_description);
            var backButton = FindViewById<Button>(Resource.Id.backButton);

            _userAvatarIv?.LoadImage(_viewModel.ProfilePictureUrl);
            if (_userAvatarIv != null)
            {
                _userAvatarIv.OutlineProvider = new CircleOutlineProvider();
                _userAvatarIv.ClipToOutline = true;
            }
            _userNameTv?.SetText(_viewModel.Name, TextView.BufferType.Normal);
            _userNicknameTv?.SetText(_viewModel.NickName, TextView.BufferType.Normal);
            _userAgeTv?.SetText(_viewModel.Age, TextView.BufferType.Normal);
            _descriptionTv?.SetText(_viewModel.Description, TextView.BufferType.Normal);

            backButton.SetOnClickListener(this);
        }

        public void OnClick(View v) => OnBackPressed();
    }
}